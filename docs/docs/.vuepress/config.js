module.exports = {
  title: 'Laravel Charts',
  description: 'The laravel adapter for Chartian',
  head: [['link', { rel: 'icon', href: '/logo.png' }]],
  themeConfig: {
    logo: '/logo.png',
    searchPlaceholder: 'Search...',
    lastUpdated: 'Last Updated',
    sidebar: [
      '/',
      '/guide/',
      '/guide/installation',
      '/guide/create_charts',
      '/guide/chart_configuration',
      '/guide/render_charts',
      '/guide/chart_customization',
    ],
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Chartian', link: 'https://chartian.dev' },
      { text: 'Github', link: 'https://github.com/ConsoleTVs/Charts' },
    ],
  },
};
