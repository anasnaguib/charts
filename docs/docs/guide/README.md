# Getting Started

Laravel charts allows to integrate Chartian into a laravel application with ease.

Chartian is a very powerful front-end library that allows creating charts and managing them
with a simple API.

This new version of Laravel Charts uses Chartian under the hood to get the best experience
possible on the back-end and front-end. You can read more about Chartian in the official
site at [Chartian](https://chartian.dev)

![Example Charts](https://image.prntscr.com/image/pwONtZIUSOGnxud9Omh4-Q.png)

Laravel charts provides features on the top of the Chartian's PHP adapter to provide a
top-notch laravel experience. This makes creating charts a laravel experience that's quick
and elegant.
