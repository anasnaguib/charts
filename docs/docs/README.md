---
home: true
heroImage: /logo.png
heroText: Laravel Charts
tagline: The laravel adapter for Chartian
actionText: Get Started →
actionLink: /guide/
features:
  - title: Chartian-Powered
    details: Powered by one of the most powerful charting libraries on the front-end.
  - title: Performant & Customizable
    details: Chartian does allow creating performant and customizable charts out of the box.
  - title: Laravel features
    details: Specific laravel features like routing, middlewares and creation commands.
footer: MIT Licensed — Copyright © 2020-present Erik C. Forés
---
